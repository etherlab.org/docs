#!/bin/sh
set -e

echo "<h2>Sphinx code documentation</h2>" >> public/index.html

grep -v -E '^(\#.*)?$' sphinx_repos.txt | while IFS=$'\t\n' read project version_major branch_name link_title; do
  if [ -z $project ]; then
    continue
  fi
  mkdir tmp; cd tmp
  wget -O artifacts.zip $CI_SERVER_URL/$CI_PROJECT_NAMESPACE/$project/-/jobs/artifacts/$branch_name/download?job=sphinx
  unzip artifacts.zip
  dir=$project/$version_major
  mkdir -p ../public/$dir
  mv html ../public/$dir/sphinx
  cd ..; rm -rf tmp
  echo "  <p><a href=\"$dir/sphinx/index.html\">$link_title</a></p>" >> public/index.html
done

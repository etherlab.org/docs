#!/bin/sh
set -e

echo "<h2>Code coverage</h2>" >> public/index.html

grep -v -E '^(\#.*)?$' coverage_repos.txt | while IFS=$'\t\n' read project version_major branch_name link_title; do
  if [ -z $project ]; then
    continue
  fi
  mkdir tmp; cd tmp
  wget -O artifacts.zip $CI_SERVER_URL/$CI_PROJECT_NAMESPACE/$project/-/jobs/artifacts/$branch_name/download?job=coverage
  unzip artifacts.zip
  mkdir -p ../public/$project/$version_major
  mv html ../public/$project/$version_major/coverage
  cd ..; rm -rf tmp
  echo "  <p><a href=\"$project/$version_major/coverage/index.html\">$link_title</a></p>" >> public/index.html
done

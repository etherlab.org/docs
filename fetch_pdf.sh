#!/bin/sh
set -e

echo "<h2>Handbooks</h2>" >> public/index.html

grep -v -E '^(#.*)?$' pdf_repos.txt | while IFS=$'\t\n' read project version_major branch_name filename description; do
  if [ -z $project ]; then
    continue
  fi
  dir=$project/$version_major
  mkdir -p public/$dir/$(dirname $filename)
  wget -O public/$dir/$filename $CI_SERVER_URL/$CI_PROJECT_NAMESPACE/$project/-/jobs/artifacts/$branch_name/raw/$filename?job=pdf
  echo "  <p><a href=\"$dir/$filename\">$description</a></p>" >> public/index.html
done

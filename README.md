[![pipeline status](https://gitlab.com/etherlab.org/docs/badges/main/pipeline.svg)](https://gitlab.com/etherlab.org/docs/-/commits/main)

# Docs page CI for etherlab.org

This is the source for https://docs.etherlab.org.

The Pipeline goes through all projects defined in [doxygen_repos.txt](doxygen_repos.txt) and [latex_repos.txt](latex_repos.txt) and collects the generated documentation.

## How to add Doxygen files from a new project

The following assumptions are made:
 - The project has a pipeline job named `doxygen`.
 - This job has only one artifact, a `html` directory.

Add a line to [doxygen_repos.txt](doxygen_repos.txt), the fields are described there. Note that tabs (`\t`) are used as separator,
so spaces are fine. Blank line and lines starting with a `#` are ignored.

## How to add Coverage files from a new project

The following assumptions are made:
 - The project has a pipeline job named `coverage`.
 - This job has only one artifact, a `html` directory.

Add a line to [coverage_repos.txt](coverage_repos.txt), the fields are described there. Note that tabs (`\t`) are used as separator,
so spaces are fine. Blank line and lines starting with a `#` are ignored.

## How to add PDF files


The following assumptions are made:
 - The project has a pipeline job named `pdf`.

Add a line to [pdf_repos.txt](pdf_repos.txt), the fields are described there. Note that tabs (`\t`) are used as separator,
so spaces are fine. Blank line and lines starting with a `#` are ignored.

## Trigger Website rebuild from a project

When changes on a project are made, this project can be triggered to rebuild the website.
In your project, add a new stage to the pipeline which runs after the documentation artifacts are made.
Sample:

```yaml

stages:
    - build
    - test
    - doc
    - deploy

...

doxygen:
    stage: doc
    script:
        - make doxygen
    artifacts:
        ...

pdf:
    stage: doc
    script:
        - make 
    artifacts:
        ...
    
update docs server:
    stage: deploy
    rules:
        - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH && $CI_PROJECT_NAMESPACE == "etherlab.org"
    trigger: etherlab.org/docs
```

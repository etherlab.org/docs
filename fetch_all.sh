#!/bin/sh

set -e

mkdir -p public
cp index.html.begin public/index.html
./fetch_doxygen.sh
./fetch_sphinx.sh
./fetch_coverage.sh
./fetch_pdf.sh
cat index.html.end >> public/index.html
